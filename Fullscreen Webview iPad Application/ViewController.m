//
//  ViewController.m
//  Fullscreen Webview iPad Application
//
//  Created by Avi Gelkop on 3/4/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIWebViewDelegate>

@property (nonatomic, strong, nullable) NSString *webViewUrl;
@property (nonatomic, strong, nullable) NSTimer *timer;

@end

@implementation ViewController {}

static NSString * const baseUrl = @"https://s3-eu-west-1.amazonaws.com/inneractive-assets/CS_Tools/getaroom_tablet_url/getaroom_tablet_location.txt";
static int const kRefreshTime = 1800; //in seconds

- (void)viewDidLoad {
    [super viewDidLoad];
    //Screen won't turn off:
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    //No scrolling and pinching
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.scalesPageToFit = NO;
    //refresh timer:
    _timer = [NSTimer scheduledTimerWithTimeInterval:kRefreshTime target:self selector:@selector(getWebViewUrl) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    
    [self getWebViewUrl];
    // know we will wait until we will get answer about the webView url and then updateWeb() should be called
}

- (NSString *)getWebViewUrl {
    NSURLSession *aSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[aSession dataTaskWithURL:[NSURL URLWithString:baseUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (((NSHTTPURLResponse *)response).statusCode == 200) {
            if (data) {
                //parse the json and get the url
                NSString * editedString = [self getUrlFromTheJson:data];
                //check if the url has changed
                if(![self.webViewUrl isEqualToString:editedString])
                {
                    self.webViewUrl = editedString;
                }
                //anyway refresh the webView
                [self updateWeb];
            }
        }
    }] resume];
    return @"";
}

//load the webView
-(void)updateWeb{
    NSLog(@"loading/refreshing the webView");
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970] * 1000.0;
        NSString *URLString = [NSString stringWithFormat:@"%@?t=%0.f", self.webViewUrl, timeStamp];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [self.webView loadRequest:URLRequest];
        //        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://fybermeetingroomstablet.netlify.com"]]];
        //        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    });
}

//parse the json that contains the webView url
-(NSString *) getUrlFromTheJson:(NSData *) returnedData{
    
    NSLog(@"parsing json");
    
    if (returnedData){
        if(NSClassFromString(@"NSJSONSerialization"))
        {
            NSError *error = nil;
            id object = [NSJSONSerialization
                         JSONObjectWithData:returnedData
                         options:0
                         error:&error];
            if(error) {
                NSLog(@"error parsing json");
            }
            if([object isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *results = object;
                return [results valueForKey:@"url"];
            }
            else
            {
                NSLog(@"error parsing json, cant handeled as dictionary");
            }
        }
        else
        {
            NSLog(@"can't using NSJSONSerialization");
        }
    }
    NSLog(@"No data from the website");
    return @"";
}

//hide the status bar
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"shouldStartLoadWithRequest: %@", request.URL.absoluteString);
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"webViewDidFinishLoad");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"didFailLoadWithError");
}

@end
