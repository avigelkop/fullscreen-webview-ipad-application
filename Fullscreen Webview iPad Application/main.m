//
//  main.m
//  Fullscreen Webview iPad Application
//
//  Created by Avi Gelkop on 3/4/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
