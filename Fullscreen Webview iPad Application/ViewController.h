//
//  ViewController.h
//  Fullscreen Webview iPad Application
//
//  Created by Avi Gelkop on 3/4/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
